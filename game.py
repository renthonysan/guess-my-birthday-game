# importing the function randint
from random import randint

username = input("Player, what is your name? ")

# Have five guesses for the player's birthday through the random num gen
for guess_number in range (5):
    month_guess = randint (1, 12) #Guessing the month
    year_guess = randint (1924, 2004) #Guessing the year
    print("Guess #",guess_number + 1,": ",username," were you born in ",month_guess, "/", year_guess, "y/n")
    user_answer = input()
    if user_answer == "y" :
        print("I knew it!")
        break
    elif guess_number == 4 :
        print("I have other things to do. Good bye.")
        break
    else :
        print("Drat! Let me try again!")
